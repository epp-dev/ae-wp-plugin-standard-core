def deprecated_step(string)
  puts 'Deprecated step. Please use following form:'
  puts string
  step string
end

Given(/^(?:|I )am on ([^\"]+)$/) do |page_name|
  deprecated_step "I am on \"#{page_name}\""
end

Given(/^(?:|I )open website (.+)$/) do |url|
  deprecated_step "I am on \"#{url}\""
end

When(/^(?:|I )go to (.+)$/) do |page_name|
  deprecated_step "I am on \"#{page_name}\""
end

Then(/^(?:|I )should be on ([^\"]+)$/) do |page_name|
  deprecated_step "I should be on \"#{page_name}\""
end

When(/^(?:|I )click link with xpath "([^"]*)"(?: within "([^"]*)")?$/) do |xpath, selector|
  deprecated_step "I click xpath \"#{xpath}\"" + (selector ? " within \"#{selector}\"" : '')
end

When(/^I click on image "([^"]*)"$/) do |element|
  deprecated_step "I click on element \"#{element}\""
end

When(/^I click on the element "([^"]*)"$/) do |element|
  deprecated_step "I click on element \"#{element}\""
end

Given(/^I hover over the "([^"]*)"$/) do |element|
  deprecated_step "I hover over \"#{element}\""
end

Then(/^I should have (?:at least )?"(\d+) ([^"]*)" within "([^"]*)"$/) do |number_of_elements, selector, parent|
  deprecated_step "I should have at least #{number_of_elements} \"#{selector}\" within \"#{parent}\""
end

Given(/^I am logged in$/) do
  page.driver.browser.credentials = 'user:pw'
end

When(/^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/) do |field, value, selector|
  deprecated_step "I fill in \"#{value}\" for \"#{field}\"" + (selector ? " within \"#{selector}\"" : '')
end

Then(/^the "([^"]*)" field(?: within "([^"]*)")? should contain "([^"]*)"$/) do |field, selector, value|
  deprecated_step "\"#{value}\" should be contained in field \"#{field}\"" + (selector ? " within \"#{selector}\"" : '')
end

Then(/^the "([^"]*)" field(?: within "([^"]*)")? should not contain "([^"]*)"$/) do |field, selector, value|
  deprecated_step "\"#{value}\" should not be contained in field \"#{field}\"" + (selector ? " within \"#{selector}\"" : '')
end

Then(/^the "([^"]*)" field(?: within "([^"]*)")? should(not )? equal "([^"]*)"$/) do |field, selector, negate, value|
  deprecated_step "\"#{value}\" should #{negate ? 'not ' : ''}be equal to field \"#{field}\"" + (selector ? " within \"#{selector}\"" : '')
end

Then(/^the "([^"]*)" checkbox(?: within "([^"]*)")? (should be|should not be) checked$/) do |field, selector, should_or_not|
  deprecated_step "#{should_or_not} checked \"#{field}\"" + (selector ? " within \"#{selector}\"" : '')
end

Then(/^I submit (?:form )?the "([^\"]*)"(?: form)?(?: within "([^"]*)")?$/) do |form_id, _selector|
  deprecated_step "I submit form \"#{form_id}\""
end
