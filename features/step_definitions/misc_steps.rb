# Hover given element.
#
# @yieldparam element [String] Css selector for element.
When(/^I hover over "([^"]*)"$/) do |element|
  element = find("#{selector_for(element)}")
  element.hover
end

# Test for css attribute value
#
Then(/^the element "([^"]*)" should have a css attribute "([^"]*)" with value "([^"]*)"$/) do |element, cssKey, value|
  page.evaluate_script("jQuery('#{selector_for(element)}').css('" + cssKey + "')").should have_content(value)
end

# Test for attribute value
#
Then(/^the element with xpath "([^"]*)" should have an attribute "([^"]*)" with (the exact )?value "([^"]*)"$/) do |element, attr, exact, value|
  xpath_attribute_compare = exact ? '[@' + attr + '="' + value + '"]' : '[contains("' + value + '", ' + attr + ')]'

  xpath_path = element + xpath_attribute_compare

  if page.respond_to? :should
    page.should have_xpath(xpath_path)
  else
    assert page.has_xpath?(xpath_path)
  end
end

# Test for attribute value
#
Then(/^the element "([^"]*)" should have an attribute "([^"]*)" with value "([^"]*)"$/) do |element, attr, value|
  page.evaluate_script("jQuery('#{selector_for(element)}').attr('" + attr + "')").should have_content(value)
end

# Test for class attribute value
#
Then(/^the element "([^"]*)" should have class "([^"]*)"$/) do |element, value|
  page.evaluate_script("jQuery('#{selector_for(element)}').hasClass('" + value + "')").should be_truthy
end

# Test for absence of class attribute value
#
Then(/^the element "([^"]*)" should not have class "([^"]*)"$/) do |element, value|
  page.evaluate_script("jQuery('#{selector_for(element)}').hasClass('" + value + "')").should be_falsey
end

Then(/^I should have exactly "(\d+) ([^"]*)" within "([^"]*)"$/) do |number_of_elements, selector, parent|
  with_scope(parent) do
    elements = all(selector_for(selector))
    elements.size.should == number_of_elements.to_i
  end
end

Then(/^I should have not more than "(\d+) ([^"]*)" within "([^"]*)"$/) do |number_of_elements, selector, parent|
  with_scope(parent) do
    elements = all(selector_for(selector))
    elements.size.should <= number_of_elements.to_i
  end
end

Then(/^the element "([^"]*)" should not be visible after waiting (\d) seconds$/) do |element, seconds|
  sleep(seconds.to_i)
  page.evaluate_script("jQuery('#{selector_for(element)}').is(':visible')").should be_falsey
end

Then(/^the element "([^"]*)" should be visible after waiting (\d) seconds$/) do |element, seconds|
  sleep(seconds.to_i)
  page.evaluate_script("jQuery('#{selector_for(element)}').is(':visible')").should be_truthy
end

And(/^all elements "([^"]*)" within "([^"]*)" should have the class "([^"]*)"$/) do |selector, parent, value|
  with_scope(parent) do
    all(selector_for(selector)).size.should == all(selector_for("#{selector}#{value}")).size
  end
end

Then(/^I native hover the element "([^"]*)"$/) do |element|
  element = selector_for(element)
  page.find(element).native.hover
end

# Test the page title
#
Then(/^I should see the page titled "([^\"]*)"$/) do |title|
  page.should have_title(title)
end

# use only for debugging
#
Then(/^I take a screenshot$/) do | |
  page.save_screenshot('./cucumber_debug_screenshot.png', full: true)
end

# Save current page.
#
Then(/^show me the page$/) do
  save_and_open_page # rubocop:disable Lint/Debugger
end

# Mark current test as pending.
#
Given(/^PENDING/) do
  pending
end

# Wait x seconds.
#
# @yieldparam seconds [String] Seconds to wait.
Then(/^I wait for (\d+) seconds*$/) do |seconds|
  sleep seconds.to_i
end

Then /^I clear cookies$/ do
  Capybara.reset_sessions!
end