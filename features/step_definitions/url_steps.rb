# Open given url.
#
# @yieldparam page_name [String] Name of page to open.
Given(/^(?:|I )am on "([^"]*)"$/) do |page_name|
  visit path_to(page_name)
  [404, 500, 403].should_not include(page.status_code)
end

# Check current url.
#
# @yieldparam page_name [String] Page to test.
Then(/^(?:|I )should be on "([^"]*)"$/) do |page_name|
  current_path = URI.parse(current_url).path
  current_path.should == path_to(page_name)
end

# Check current url query parameters.
#
# @yieldparam expected_pairs [String] GET Parameter to test.
Then(/^(?:|I )should have the following query string:$/) do |expected_pairs|
  query = URI.parse(current_url).query
  actual_params = query ? CGI.parse(query) : {}
  expected_params = {}
  expected_pairs.rows_hash.each_pair { |k, v| expected_params[k] = v.split(',') }
  actual_params.should == expected_params
end
