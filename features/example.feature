@smoke_test
Feature: Check default settings of website against specifications
  In order to ensure the specifications
  Visitors should be able to use the website as expected

  Scenario: Check footer
    Given I am on "Homepage"
    Then I should see "ae-wp-plugin-standard.org" within ".header"
