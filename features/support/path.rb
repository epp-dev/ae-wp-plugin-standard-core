# Navigation helper module for cucumber steps
#
module NavigationHelpers
  # Mapping for path names to real remote path
  #
  # @param path [String]
  # @return [String]
  def path_to(page_name)
    case page_name
      when 'Homepage'
        '/'
      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(NavigationHelpers)

