# Capybara configuration (using Qt)
require 'uri'
require 'cgi'
require 'capybara'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/session'
require 'phantomjs'
require 'capybara/poltergeist'

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(
      app,
      window_size: [1600, 900],
      phantomjs: Phantomjs.path,
      phantomjs_options: %w(--ignore-ssl-errors=true --ssl-protocol=any --disk-cache=false),
      js_errors: false
  )
end

# RSpec
require 'rspec/expectations'

# TestUnit
require 'test/unit/assertions'

unless ENV['CUCUMBER_HOST']
  puts %Q{
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
You should provide the argument CUCUMBER_HOST in order to run cucumber tests or set 'Capybara.app_host' in your env.rb file.
Examples:
  cucumber CUCUMBER_HOST=http://www.example.com/
  Capybara.app_host = "http://www.example.com/"
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

}
  abort
end


Capybara.default_driver = :poltergeist
Capybara.javascript_driver = :poltergeist
Capybara.default_selector = :css
Capybara.run_server = false
Capybara.app_host = ENV['CUCUMBER_HOST']

#change back to old default settings Capybara 2.0.2
Capybara.ignore_hidden_elements = false
Capybara.visible_text_only = true

# register Capybara
World(Test::Unit::Assertions)
World(Capybara)
