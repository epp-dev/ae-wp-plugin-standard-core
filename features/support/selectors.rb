# Cucumber step helper function for selector_for
#
module HtmlSelectorsHelpers
  # Maps a name to a selector. Used primarily by the
  #
  #   When /^(.+) within (.+)$/ do |step, scope|
  #
  # step definitions in web_steps.rb
  #
  def selector_for(locator)
    case locator
      when 'title'
        [:xpath, '/html/head/title']
      when /"(.+)"/
        $1
      else
        locator
    end
  end
end

World(HtmlSelectorsHelpers)
