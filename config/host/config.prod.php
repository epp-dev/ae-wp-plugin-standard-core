<?php

use AEWPPluginStandard\Enum\ModulesEnum;

return [
    'baseUrl' => 'https://www.ae-wp-plugin-standard.org',
    'pids' => [
        ModulesEnum::EXAMPLE => [
            1,    // home page
        ],
    ]
];
