<?php

use AEWPPluginStandard\Enum\ModulesEnum;

return [
    'baseUrl' => 'http://ae-wp-plugin-standard.dev',
    'pids' => [
        ModulesEnum::EXAMPLE => [
            1,    // home page
        ]
    ]
];
