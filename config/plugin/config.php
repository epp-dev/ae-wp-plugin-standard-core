<?php

return [
    'modules' => [
        \AEWPPluginStandard\Enum\ModulesEnum::EXAMPLE => [
            'defaults' => [
                'example-setting' => true
            ]
        ]
    ]
];
