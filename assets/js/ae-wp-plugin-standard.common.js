/*
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 19.09.17 14:15
 */

/*global Marionette, Backbone */
(function () {
    'use strict';
    var AEWPPluginExampleModule = {
        moduleName: 'ae-wp-plugin-standard.common',
        load: function () {
            AEWPPluginExampleModule.registerEventHandlers();
        },
        isHttpParameterSet: function (parameterName) {
            var re = new RegExp('[?&]\\b' + parameterName + '\\b', 'g');
            return re.test(location.search);
        },
        getHttpParameter: function (parameterName) {
            var parameters = location.search.replace('?', '').split('&'),
                parameter,
                parameterValue = null;
            for (var i = 0; i < parameters.length; i++) {
                parameter = parameters[i].split('=');
                if (parameter[0] === parameterName) {
                    parameterValue = parameter[1];
                    break;
                }
            }
            return parameterValue;
        },
        setCookie: function (name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires === 'number' && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            } else {
                expires = AEWPPluginExampleModule.cookieTTL;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + '=' + value;

            for (var propName in options) {
                if (options.hasOwnProperty(propName)) {
                    updatedCookie += '; ' + propName;
                    var propValue = options[propName];
                    if (propValue !== true) {
                        updatedCookie += '=' + propValue;
                    }
                }
            }

            document.cookie = updatedCookie;
        },
        /**
         *
         * @param cookieName string
         * @returns {string}
         */
        getCookie: function (cookieName) {
            var name = cookieName + '=',
                decodedCookie = decodeURIComponent(document.cookie),
                ca = decodedCookie.split(';'),
                c, i;
            for (i = 0; i < ca.length; i++) {
                c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return '';
        },
        /**
         * @returns {{}}
         */
        getAllUrlParams: function () {
            var url = window.location.search,
                request = {},
                pairs = url.substring(url.indexOf('?') + 1).split('&'),
                i,
                pair;
            for (i = 0; i < pairs.length; i++) {
                if (!pairs[i]) {
                    continue;
                }
                pair = pairs[i].split('=');
                request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            }
            return request;
        },
        /**
         * @param obj
         * @returns {boolean}
         */
        isEmptyObject: function (obj) {
            var name;
            for (name in obj) {
                if (obj.hasOwnProperty(name)) {
                    return false;
                }
            }
            return true;
        },
        serializeObject: function (obj, prefix) {
            var str = [], p, k, v;
            for (p in obj) {
                if (obj.hasOwnProperty(p)) {
                    k = prefix ? prefix + '[' + p + ']' : p;
                    v = obj[p];
                    str.push((v !== null && typeof v === 'object') ?
                        this.serializeObject(v, k) :
                        encodeURIComponent(k) + '=' + encodeURIComponent(v));
                }
            }
            return str.join('&');
        },
        toggleOverlayOf: function (overlayUs) {
            var overlay, overlayClassList;
            if (overlayUs instanceof NodeList) {
                Array.prototype.forEach.call(overlayUs, function (overlayMe) {
                    overlay = overlayMe.querySelector('.loading-overlay');
                    if (overlay) {
                        // remove overlay
                        overlayMe.removeChild(overlay);
                    } else {
                        // add overlay
                        overlay = document.createElement('div');
                        overlayClassList = overlay.classList;

                        overlayClassList.add('loading-overlay');
                        overlayClassList.add('centered-overlay');

                        overlay.style.width = overlayMe.offsetWidth + 'px';
                        overlay.style.height = overlayMe.offsetHeight + 'px';
                        overlay.style.top = overlayMe.offsetTop + 'px';
                        overlay.style.left = overlayMe.offsetLeft + 'px';

                        overlayMe.insertBefore(overlay, overlayMe.firstChild);
                    }
                });
            } else {
                overlay = overlayUs.querySelector('.loading-overlay');
                if (overlay) {
                    // remove overlay
                    overlayUs.removeChild(overlay);
                } else {
                    // add overlay
                    overlay = document.createElement('div');
                    overlayClassList = overlay.classList;

                    overlayClassList.add('loading-overlay');
                    overlayClassList.add('centered-overlay');

                    overlay.style.width = overlayUs.offsetWidth + 'px';
                    overlay.style.height = overlayUs.offsetHeight + 'px';
                    overlay.style.top = overlayUs.offsetTop + 'px';
                    overlay.style.left = overlayUs.offsetLeft + 'px';

                    overlayUs.insertBefore(overlay, overlayUs.firstChild);
                }
            }

            return window.CchCommon;
        },
        appendQueryString: function(queryString){
            if (history.pushState) {
                var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + queryString;
                window.history.replaceState(
                    {
                        path: newUrl
                    },
                    '',
                    newUrl
                );
            }
        },
        registerEventHandlers: function () {
            return AEWPPluginExampleModule;
        },
    };

    AEWPPluginExampleModule.load();
})();