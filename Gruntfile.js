/* global module, require */

module.exports = function (grunt) {
    'use strict';
    require('load-grunt-tasks')(grunt);
    grunt.loadTasks('lib/grunt/tasks');

    grunt.registerTask('deploy', function (stage) {
        var withLinter = grunt.option('with-linter'),
            optimize = grunt.option('optimize'),
            withVendor = grunt.option('vendor');

        if (stage === 'dev' || !stage) {
            grunt.task.run('ae-composer-dump-autoload');

            /**
             * run linter if parameter set
             */
            if (withLinter) {
                grunt.task.run('ae-lint');
            }

            // copy files to dist directory that don't need to be preprocessed
            grunt.task.run('ae-copy-to-dist');

            /**
             * combine and minify css
             */
            grunt.task.run('ae-minify-css');

            /**
             * bump plugin version in dist directory
             */
            grunt.task.run('ae-plugin-version-bump');

            /**
             * copy sources from dist to develop stage
             */
            grunt.task.run('ae-deploy-to-dev');

            if (withVendor) {
                grunt.task.run('ae-copy-vendor-to-dev');
            }
        } else if (stage === 'production' || stage === 'testing') {
            /**
             * run linter if parameter set
             */
            if (withLinter) {
                grunt.task.run('ae-lint');
            }

            grunt.task.run('ae-test');

            // copy files to dist directory that don't need to be preprocessed
            grunt.task.run('ae-copy-to-dist');

            /**
             * combine and minify css
             */
            grunt.task.run('ae-minify-css');

            /**
             * bump plugin version in dist directory
             */
            grunt.task.run('ae-plugin-version-bump');

            /**
             * @todo: doesn't work yet, problems with ssh and sudo
             */
            // grunt.task.run('ae-deploy-production');
        }
    });
};