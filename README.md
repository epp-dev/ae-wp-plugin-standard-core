## **Development** ##

### Set upstream and merge ###

```
    #!bash

    git remote add upstream git@bitbucket.org:epp-dev/ae-wp-plugin-standard-core.git
    git fetch upstream
    git checkout master
    git merge upstream/master

```

### Replace by your plugin name ###

Following strings should be set/replaced by your plugin name. Order is important:

| String | Meaning or usage examples |
| ------ | ------ |
| AE WP Plugin Standard Core | composer.json project description; can be equal to the next |
| AE WP Plugin Standard | "Nice" name of your plugin, global AE_WP_PLUGIN_STANDARD_NAME |
| AEWPPluginStandard    | Namespace, global AE_WP_PLUGIN_STANDARD_TECHNICAL_NAME |
| aeWpPluginStandard    | Function prefix, variable name prefix |
| https://bitbucket.org/epp-dev/ae-wp-plugin-standard-core | repo, tracker, wiki urls |
| ae-wp-plugin-standard/core | composer.json project name |  
| ae-wp-plugin-standard-core | package.json project name |
| ae-wp-plugin-standard | Prefix of WP hooks |

Also look for directory and file names e.g.:

* app/src/AEWPPluginStandard
* app/ae-wp-plugin-standard.php
* assets/scripts/ae-wp-plugin-standard/ae-wp-plugin-standard.example.js

### Getting started ###

* Install composer in lib directory following the instructions here: https://getcomposer.org/download/
* Install composer dependencies

```
  #!bash
  
  php lib/composer.phar --no-interaction --ansi install
```
  
* Install node:
* Install node dependencies:

```
#!bash
  
npm install
```
* Install Ruby and RVM:
* Install Ruby dependencies:
 
```
#!bash

bundle install
```

* Install Bower:
* Install Bower dependencies:

```
#!bash

bower install
```

## **Grunt tasks** ##

### **Versioning** ###
Commits erfolgen über **IntelliJ IDEA** 

| Description | Command |
| ------ | ------ | 
|Push and patch version bump (0.0.**x**)| `grunt ae-push --change=patch` |
|Push and minor (default) version bump (0.**x**.0)| `grunt ae-push` |
|Push and major version bump (**x**.0.0)| `grunt ae-push --change=major` |

### **Build & deploy** ###

| Description | Command  |PhpStorm Shortcut|
| ------ | ------ | ------ |
|Build & deploy without uglifying|`grunt deploy`|`F9`|
|Build & deploy and uglify js|`grunt deploy --optimize`||


## **Shortcodes** ##


`[example-shortcode-name example-attribute=xxx]`

## **Setup** ##

### Dependencies ###
* [Bootstrap v3](http://getbootstrap.com/)
* [node.js](http://nodejs.org/)
    * [Grunt](http://gruntjs.com/)
        * [grunt-bumpup](https://github.com/darsain/grunt-bumpup)
        * [grunt-contrib-clean](https://github.com/gruntjs/grunt-contrib-clean)
        * [grunt-contrib-cssmin](https://github.com/gruntjs/grunt-contrib-cssmin)
        * [grunt-contrib-copy](https://github.com/gruntjs/grunt-contrib-copy)
        * [grunt-git](https://github.com/rubenv/grunt-git)
* [jQuery](http://jquery.com/)
* [WP CLI](https://wp-cli.org)
    * [WP Rocket CLI](https://github.com/GeekPress/wp-rocket-cli)