/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-plugin-version-bump', 'Increment plugin version', function () {
        grunt.config.init({
            replace: {
                dist: {
                    options: {
                        patterns: [
                            {
                                match: 'version',
                                replacement: grunt.file.readJSON('package.json').version
                            }
                        ]
                    },
                    files: [
                        {src: ['dist/prod/ae-wp-plugin-standard.php'], dest: './'},
                        {src: ['dist/testing/ae-wp-plugin-standard.php'], dest: './'}
                    ]
                }
            }
        });
        grunt.task.run('replace');
    });
};