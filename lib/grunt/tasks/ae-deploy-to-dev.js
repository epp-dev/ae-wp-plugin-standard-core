/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-deploy-to-dev', 'Deploy release to the DEV stage', function () {
        grunt.config.init({
            clean: {
                options: {
                    force: true
                },
                default: {
                    expand: true,
                    cwd: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard',
                    src: [
                        '**/*',
                        '!vendor',
                        '!vendor/**/*'
                    ]
                }
            },
            copy: {
                default: {
                    files: [
                        {
                            cwd: 'dist/prod/',
                            src: ['**/*', '!config/host/config.php'],
                            dest: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard/',
                            expand: true
                        },
                        {
                            cwd: 'config/host/',
                            src: 'config.dev.php',
                            dest: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard/config/host/',
                            expand: true,
                            rename: function (dest, src) {
                                return dest + src.replace('.dev', '');
                            }
                        }
                    ]
                },
                composerAutoload: {
                    files: [
                        {
                            cwd: 'vendor',
                            src: ['composer/**/*', 'autoload.php'],
                            dest: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard/vendor',
                            expand: true,
                            force: true,
                            dot: true
                        }
                    ]
                }
            }
        });
        grunt.task.run('clean');
        grunt.task.run('copy');
    });
};