/* global module, console */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-push', 'Bump up the version and push to GIT repository', function () {
        var change = grunt.option('change'), version = grunt.file.readJSON('package.json').version,
            versionParts = version.split('.');
        change = change ? change : 'minor';

        switch (change) {
            case 'major':
                versionParts[0] = parseInt(versionParts[0]) + 1;
                break;
            case 'minor':
                versionParts[1] = parseInt(versionParts[1]) + 1;
                break;
            case 'patch':
                versionParts[2] = parseInt(versionParts[2]) + 1;
                break;
        }

        if(change === 'minor'){
            versionParts[2] = 0;
        } else if(change === 'major'){
            versionParts[1] = 0;
            versionParts[2] = 0;
        }

        version = versionParts.join('.');
        console.log('Version bump to ' + version);
        grunt.config.init({
            gitpush: {
                pushToRemote: {
                    options: {
                        remote: 'origin',
                        verbose: true,
                        force: false
                    }
                }
            },
            bumpup: {
                options: {
                    dateformat: 'YYYY-MM-DD HH:mm',
                    normalize: false
                },
                file: 'package.json'
            },
            gitcommit: {
                commitChanges: {
                    options: {
                        message: 'Version bump to ' + version,
                        verbose: true,
                        allowEmpty: false
                    },
                    files: {
                        src: 'package.json'
                    }
                }
            }
        });
        grunt.task.run('bumpup:' + change);
        grunt.task.run('gitcommit');
        grunt.task.run('gitpush');
    });
};