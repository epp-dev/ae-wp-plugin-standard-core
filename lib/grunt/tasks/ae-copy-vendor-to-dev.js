/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-copy-vendor-to-dev', 'Helper task to copy vendor files to the DEV stage', function () {
        grunt.config.init({
            clean: {
                options: {
                    force: true
                },
                default: {
                    expand: true,
                    cwd: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard',
                    src: [
                        'vendor/**/*'
                    ]
                }
            },
            copy: {
                default: {
                    files: [
                        {
                            src: ['vendor/**/*'],
                            dest: '/Users/aleksandr/Sites/ae-wp-plugin-standard/wp-content/plugins/ae-wp-plugin-standard/',
                            expand: true
                        },
                    ]
                }
            }
        });
        grunt.task.run('clean');
        grunt.task.run('copy');
    });
};