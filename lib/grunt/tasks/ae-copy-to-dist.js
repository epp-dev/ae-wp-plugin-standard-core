/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-copy-to-dist', 'Build release for deployment without css files', function () {
        grunt.config.init({
            copy: {
                php: {
                    files: [
                        {
                            src: 'app/src/**/*',
                            dest: 'dist/prod/',
                            expand: true
                        },
                        {
                            src: 'app/src/**/*',
                            dest: 'dist/testing/',
                            expand: true
                        },
                        {
                            expand: true,
                            cwd: 'app/',
                            src: 'ae-wp-plugin-standard.php',
                            dest: 'dist/prod/',
                            flatten: true
                        },
                        {
                            expand: true,
                            cwd: 'app/',
                            src: 'ae-wp-plugin-standard.php',
                            dest: 'dist/testing/',
                            flatten: true
                        }
                    ]
                },
                js: {
                    files: [
                        {
                            src: 'assets/js/**/*',
                            dest: 'dist/prod/',
                            expand: true
                        },
                        {
                            src: 'assets/js/**/*',
                            dest: 'dist/testing/',
                            expand: true
                        },
                    ]
                },
                configs: {
                    files: [
                        {
                            src: 'config/host/config.testing.php',
                            dest: 'dist/testing/',
                            expand: true,
                            rename: function (dest, src) {
                                return dest + src.replace('.testing', '');
                            }
                        },
                        {
                            src: 'config/host/config.prod.php',
                            dest: 'dist/prod/',
                            expand: true,
                            rename: function (dest, src) {
                                return dest + src.replace('.prod', '');
                            },
                        },
                        {
                            src: 'config/plugin/config.php',
                            dest: 'dist/prod/'
                        },
                        {
                            src: 'config/plugin/config.php',
                            dest: 'dist/testing/'
                        }
                    ]
                },
                libs: {
                    files: [
                        {
                            src: 'package.json',
                            dest: 'dist/prod/'
                        },
                        {
                            src: 'package.json',
                            dest: 'dist/testing/'
                        },
                        {
                            src: 'lib/composer.phar',
                            dest: 'dist/prod/'
                        },
                        {
                            src: 'lib/composer.phar',
                            dest: 'dist/testing/',
                        },
                        {
                            src: 'composer.json',
                            dest: 'dist/prod/'
                        },
                        {
                            src: 'composer.json',
                            dest: 'dist/testing/'
                        }
                    ]
                },
                images: {
                    files: [
                        {
                            src: 'assets/images/**/*',
                            dest: 'dist/prod/',
                            expand: true
                        },
                        {
                            src: 'assets/images/**/*',
                            dest: 'dist/testing/',
                            expand: true
                        }
                    ]
                },
                views: {
                    files: [
                        {
                            src: 'app/views/**/*',
                            dest: 'dist/prod/',
                            expand: true
                        },
                        {
                            src: 'app/views/**/*',
                            dest: 'dist/testing/',
                            expand: true
                        }
                    ]
                },
                fonts: {
                    files: [
                        // bootstrap fonts
                        {
                            expand: true,
                            cwd: 'assets/vendor/bootstrap/fonts/',
                            src: '**',
                            dest: 'dist/prod/assets/fonts/bootstrap/'
                        },
                        {
                            expand: true,
                            cwd: 'assets/vendor/bootstrap/fonts/',
                            src: '**',
                            dest: 'dist/testing/assets/fonts/bootstrap/'
                        },
                    ]
                },
                notCombinedJs: {
                    files: [
                        {
                            expand: true,
                            cwd: 'assets/scripts/ae-wp-plugin-standard/',
                            flatten: true,
                            src: 'ae-wp-plugin-standard.example.js',
                            dest: 'dist/prod/assets/scripts/'
                        },
                        {
                            expand: true,
                            cwd: 'assets/scripts/ae-wp-plugin-standard/',
                            flatten: true,
                            src: 'ae-wp-plugin-standard.example.js',
                            dest: 'dist/testing/assets/scripts/'
                        }
                    ]
                }
            },
            clean: {
                default: ['dist/**/*']
            }
        });
        grunt.task.run('clean');
        grunt.task.run('copy');
    });
};