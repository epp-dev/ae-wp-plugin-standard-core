/*
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 25.08.17 13:37
 */

/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-composer-dump-autoload', 'Update composer autoload', function () {
        grunt.config.init({
            shell: {
                phpComposer: {
                    command: 'php lib/composer.phar dump-autoload'
                }
            }
        });
        grunt.task.run('shell');
    });
};