/* global module, require */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-lint', 'Run javascript linter and PHP Code Sniffer', function () {
        let directoriesConfig = {
            composer: 'vendor',
            composerBin: 'vendor/bin',
            reports: 'log',
            php: 'app',
            js: 'assets/scripts/ae-wp-plugin-standard',
            config: 'config',
            gruntTasks: 'lib/grunt/tasks'
        };
        grunt.initConfig({
            directories: directoriesConfig,
            jshint: {
                options: {
                    jshintrc: 'lib/grunt/tasks/.jshintrc',
                    reporter: require('jshint-stylish'),
                    reporterOutput: 'log/jshint/jshint_reporter.log',
                    // ignores: ['<%= directories.js %>/ignore.me.js']
                },
                all: [
                    'Gruntfile.js',
                    '<%= directories.gruntTasks %>/*.js',
                    '<%= directories.js %>/*.js'
                ]
            },
            jsvalidate: {
                files: [
                    'Gruntfile.js',
                    '<%= directories.gruntTasks %>/*.js',
                    '<%= directories.js %>/*.js'
                ]
            },
            jsonlint: {
                files: [
                    '*.json'
                ]
            },
            shell: {
                phpcbf: {
                    command: '<%= directories.composerBin %>/phpcbf --standard=<%= directories.config %>/ruleset.xml --report=code --warning-severity=8 --error-severity=1'
                },
                phpcs: {
                    command: [
                        'mkdir -p <%= directories.reports %>/phpcs',
                        '<%= directories.composerBin %>/phpcs --standard=<%= directories.config %>/ruleset.xml --report=code --report-file=<%= directories.reports %>/phpcs/report.log --warning-severity=8 --error-severity=1'
                    ].join(' && ')
                }
            }
        });

        grunt.task.run('jsvalidate');
        grunt.task.run('jshint');
        grunt.task.run('jsonlint');
        grunt.task.run('shell');
    });
};