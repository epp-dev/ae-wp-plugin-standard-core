/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-test', 'Run PHPUnit tests', function () {
        let directoriesConfig = {
            composerBin: 'vendor/bin',
            config: 'config'
        };

        grunt.initConfig({
            directories: directoriesConfig,
            shell: {
                phpunit: {
                    command: '<%= directories.composerBin %>/phpunit -c <%= directories.config %>/phpunit.xml'
                }
            }
        });
        grunt.task.run('shell');
    });
};