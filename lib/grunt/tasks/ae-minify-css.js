/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-minify-css', 'Combine and minify CSS files', function () {
        grunt.config.init({
            sass: {
                dist: {
                    options: {
                        style: 'expanded'
                    },
                    files: [
                        {
                            expand: true,
                            cwd: 'assets/scss',
                            src: ['main.scss'],
                            dest: 'assets/scss',
                            ext: '.css'
                        }
                    ]
                }
            },
            cssmin: {
                minify: {
                    files: [
                        {
                            expand: true,
                            flatten: true,
                            cwd: 'assets/scss',
                            src: 'main.css',
                            dest: 'dist/prod/assets/css'
                        }
                    ]
                }
            },
            copy: {
                default: {
                    files: [
                        {
                            cwd: 'dist/prod/assets/css/',
                            src: 'main.css',
                            dest: 'dist/testing/assets/css/',
                            expand: true
                        }
                    ]
                }
            }
        });
        grunt.task.run('sass');
        grunt.task.run('cssmin');
        grunt.task.run('copy');
    });
};