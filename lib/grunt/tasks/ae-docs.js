/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-docs', 'Generate Documentation with PhpDocumentor2', function () {
        let directoriesConfig = {
            composerBin: 'vendor/bin',
            reports: 'log',
            phpdocConfig: 'config/phpdoc.xml'
        };
        grunt.initConfig({
            directories: directoriesConfig,
            shell: {
                phpdocumentor: {
                    command: '<%= directories.composerBin %>/phpdoc run -c <%= directories.phpdocConfig %>'
                },
                phploc: {
                    command: [
                        'mkdir -p <%= directories.reports %>/phploc',
                        '<%= directories.composerBin %>/phploc --log-xml <%= directories.reports %>/phploc/<%= grunt.template.today("isoDateTime") %>.xml <%= directories.php %>'
                    ].join('&&')
                }
            }
        });
        grunt.task.run('shell');
    });
};
