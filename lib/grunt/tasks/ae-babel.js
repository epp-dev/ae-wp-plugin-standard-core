/* global module */
module.exports = function (grunt) {
    'use strict';
    grunt.registerTask('ae-babel', 'Transpile javascript', function () {
        grunt.config.init({
            babel: {
                options: {
                    sourceMap: false,
                    presets: ['es2015']
                },
                dist: {
                    files: {
                        'dest': 'src'
                    }
                }
            }
        });
        grunt.task.run('babel');
    });
};