#!/bin/bash

baseUrl=testing.ae-wp-plugin-standard.org
commonPath=/var/www/ae-wp-plugin-standard/
application=htdocs_testing
plugin=ae-wp-plugin-standard
user=
group=
htaccess_user=
htaccess_password=

echo "Start deployment to testing server"

grunt deploy:testing --with-linter

ssh -t ${baseUrl} "echo \"Set access rights for deployment\";
cd ${commonPath}${application};
sudo chown -R ${user}:${group} *;
sudo chmod -R 775 deployment;"

cap testing deploy
cap testing utils:upload_file['config/deploy/util/clear_opcache.php']

ssh -t ${baseUrl} "echo \"Set access rights for deployment\";
cd ${commonPath}${application};
sudo chown -R ${user}:${group} *;
sudo chmod -R 775 *;

cd deployment/current;
php lib/composer.phar --no-interaction --ansi --no-dev --optimize-autoloader --ignore-platform-reqs install

echo \"Symlink plugin and set permissions\";
cd ${commonPath}${application}/wp-content/plugins;
sudo rm ${plugin};
ln -s ${commonPath}${application}/deployment/current ${plugin};
sudo chown -h ${user}:${group} ${plugin};

echo \"Clear caches\";
cd ${commonPath}${application};

echo \"Clear WP Rocket cache\"
yes | wp rocket clean

echo \"Clear OPCache\";
sudo mv wp-content/plugins/${plugin}/config/deploy/util/clear_opcache.php wp-content/uploads;
echo \"curl -I https://www.${baseUrl}/clear_opcache.php;\"
curl -u ${htaccess_user}:${htaccess_password} -I https://www.${baseUrl}/wp-content/uploads/clear_opcache.php;

echo \"Clean up\";
sudo rm -rf wp-content/plugins/${plugin}/config/deploy;
sudo rm clear_opcache.php

sudo find . -type d -exec chmod 755 {} +;sudo find . -type f -exec chmod 644 {} +;sudo chmod 600 wp-config.php;"

cucumber CUCUMBER_HOST=https://www."${baseUrl}"
