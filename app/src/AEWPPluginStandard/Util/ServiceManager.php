<?php

namespace AEWPPluginStandard\Util;

use AEWPPluginStandard\Service\BaseService;
use InvalidArgumentException;
use AEWPPluginStandard\Enum\Messages;

if (!\class_exists(__NAMESPACE__ . 'ServiceManager')) {
    /**
     * Class ServiceManager
     * @package AEWPPluginStandard\Util
     */
    class ServiceManager
    {

        /**
         * @param string|int $type service type
         * @return BaseService
         */
        public function getService($type)
        {
            $s = null;
            switch ($type) {
                case 'base':
                    $s = new BaseService();
                    break;
                default:
                    throw new InvalidArgumentException(Messages::EXAMPLE_EXCEPTION_MESSAGE);
            }

            return $s;
        }
    }

}
