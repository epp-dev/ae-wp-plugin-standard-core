<?php

namespace AEWPPluginStandard\Util;

if (!\class_exists(__NAMESPACE__ . 'Configuration')) {
    /**
     * Class Configuration
     * @package AEWPPluginStandard\Util
     */
    class Configuration
    {

        private $pluginConfiguration;
        private $hostConfiguration;

        private $configuration;

        public function __construct()
        {
            $this->pluginConfiguration = require dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config/plugin/config.php';
            $this->hostConfiguration = require dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config/host/config.php';
            $this->configuration = array_merge($this->hostConfiguration, $this->pluginConfiguration);
        }

        public function getBaseUrl()
        {
            return $this->configuration['baseUrl'];
        }

        public function getPID($moduleName)
        {
            return $this->configuration['pids'][$moduleName];
        }

        public function getGTMID()
        {
            return $this->configuration['gtm']['id'];
        }

        public function getUploadsDir()
        {
            return $this->hostConfiguration['uploads-dir'];
        }
    }
}
