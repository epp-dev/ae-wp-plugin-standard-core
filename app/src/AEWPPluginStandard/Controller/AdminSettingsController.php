<?php
/**
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 12.09.17 20:44
 */

namespace AEWPPluginStandard\Controller;

use AEWPPluginStandard\Enum\AdminSettingsFieldsEnum;
use AEWPPluginStandard\Enum\Messages;
use AEWPPluginStandard\Plugin;

/**
 * Class AdminSettingsController
 * @package AEWPPluginStandard\Controller
 */
class AdminSettingsController extends BaseController
{
    /**
     * custom option and settings
     */
    public function initSettings()
    {
        $fieldsProperties = [
            [
                'id' => AdminSettingsFieldsEnum::FIELD1_NAME,
                'label' => Messages::FIELD1_LABEL,
                'callback' => 'fieldSingleLineCallback'
            ],
            [
                'id' => AdminSettingsFieldsEnum::FIELD2_NAME,
                'label' => Messages::FIELD2_LABEL,
                'callback' => 'fieldSingleLineCallback'
            ],
            [
                'id' => AdminSettingsFieldsEnum::FIELD3_NAME,
                'label' => \htmlspecialchars(Messages::FIELD3_LABEL),
                'callback' => 'fieldSingleLineCallback'
            ]
        ];
        // register a new setting for the plugin settings page
        \register_setting(Plugin::PREFIX . 'option_group', AdminSettingsFieldsEnum::PLUGIN_OPTIONS);

        // register a new section in the plugin settings page
        \add_settings_section(
            AdminSettingsFieldsEnum::SECTION_ID,
            __(AdminSettingsFieldsEnum::SECTION_NAME, Plugin::PREFIX),
            [$this, 'sectionCallback'],
            Plugin::PREFIX . 'settings'
        );

        foreach ($fieldsProperties as $fieldProperties) {
            // register a new field in the "plugin_section_developers" section,
            // inside the plugin settings page
            \add_settings_field(
                $fieldProperties['id'], // as of WP 4.6 this value is used only internally
                // use $args' label_for to populate the id inside the callback
                __($fieldProperties['label'], Plugin::PREFIX),
                [$this, $fieldProperties['callback']],
                Plugin::PREFIX . 'settings',
                AdminSettingsFieldsEnum::SECTION_ID,
                [
                    'label_for' => $fieldProperties['id'],
                    'class' => Plugin::PREFIX . 'row',
                    Plugin::PREFIX . 'custom_data' => 'custom',
                ]
            );
        }
    }

    /**
     * Section callback
     *
     * section callbacks can accept an $args parameter, which is an array.
     * the values are defined at the add_settings_section() function.
     *
     * @param array $args have the following keys defined: title, id, callback.
     */
    public function sectionCallback($args)
    {
        echo
            // @formatter:off
            '<p id="' . \esc_attr($args['id']) . '">' .
                \esc_html_e(Messages::SECTION_DESCRIPTION, Plugin::PREFIX) .
            '</p>';
            // @formatter:on
    }

    /**
     * field callback
     *
     * field callbacks can accept an $args parameter, which is an array.
     * $args is defined at the add_settings_field() function.
     * wordpress has magic interaction with the following keys: label_for, class.
     * the "label_for" key value is used for the "for" attribute of the <label>.
     * the "class" key value is used for the "class" attribute of the <tr> containing the field
     * you can add custom key value pairs to be used inside your callbacks.
     *
     * @param array $args
     */

    public function fieldSingleLineCallback($args)
    {
        // get the value of the setting we've registered in initSettings()
        $options = \get_option(AdminSettingsFieldsEnum::PLUGIN_OPTIONS);
        // output the field
        echo $this->renderTemplate(
            'settings/single-line.php',
            [
                'args' => $args,
                'options' => $options
            ],
            'always'
        );
    }

    /**
     * top level menu
     */
    public function optionsPage()
    {
        // add top level menu page
        \add_menu_page(
            AE_WP_PLUGIN_STANDARD_NAME . ' settings',
            AE_WP_PLUGIN_STANDARD_NAME,
            'manage_options',
            Plugin::PREFIX . 'settings',
            [$this, 'optionsPageHtml']
        );
    }

    /**
     * top level menu:
     * callback functions
     */
    public function optionsPageHtml()
    {
        // add error/update messages

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if (isset($_GET['settings-updated'])) {
            // add settings saved message with the class of "updated"
            \add_settings_error(
                Plugin::PREFIX . '_messages',
                Plugin::PREFIX . '_message',
                __('Settings saved', Plugin::PREFIX),
                'updated'
            );
        }

        // show error/update messages
        \settings_errors(Plugin::PREFIX . 'messages');
        ?>
        <div class="wrap">
            <h1><?php echo \esc_html(\get_admin_page_title()); ?></h1>
            <form action="options.php" method="post">
                <?php
                // output security fields for the registered setting
                \settings_fields(Plugin::PREFIX . 'option_group');
                // output setting sections and their fields
                // (sections are registered for settings, each field is registered to a specific section)
                \do_settings_sections(Plugin::PREFIX . 'settings');
                // output save settings button
                \submit_button('Save');
                ?>
            </form>
        </div>
        <?php
    }


    /**
     * Register callbacks for actions and filters
     */
    public function registerHookCallbacks()
    {
        /**
         * register our initSettings to the admin_init action hook
         */
        \add_action('admin_init', [$this, 'initSettings']);

        /**
         * register our optionsPage to the admin_menu action hook
         */
        \add_action('admin_menu', [$this, 'optionsPage']);
    }
}
