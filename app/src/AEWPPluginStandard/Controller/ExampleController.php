<?php
/**
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 24.08.17 22:04
 */

namespace AEWPPluginStandard\Controller;

use AEWPPluginStandard\Entity\Product;
use AEWPPluginStandard\Service\BaseRestfulService;
use AEWPPluginStandard\Service\ProductStatus;
use AEWPPluginStandard\Enum\Messages;
use AEWPPluginStandard\Util\ServiceManager;

/**
 * Class ExampleController
 * @package AEWPPluginStandard\Controller
 */
class ExampleController extends BaseController
{
    /**
     * @var boolean
     */
    private $isAjax;

    /**
     * @param $attributes
     * @return string
     */
    public function exampleShortcode($attributes)
    {
        $shortCodeAttribute = 'base';

        if (isset($attributes['example-attribute'])) {
            $shortCodeAttribute = $attributes['example-attribute'];
        }

        $service = $this->getServiceManager()->getService($shortCodeAttribute);

        $content = $service->renderTemplate(
            'example/index.php',
            [
                'shortCodeAttribute' => $shortCodeAttribute
            ],
            'once'
        );


        if ($this->isAjax) {
            echo
            exit(0);
        }

        return
            // @formatter:off
            '<div id="ae-wp-plugin-standard-content-container" '.
                $content  .
            '</div>';
            // @formatter:on
    }

    public function exampleAjaxCallback()
    {
        $this->isAjax = true;
        $this->exampleShortcode(
            [
                'example-attribute' => \filter_input(INPUT_POST, 'example-post-data')
            ]
        );
    }

    /**
     * Register callbacks for actions and filters
     */
    public function registerHookCallbacks()
    {
        $shortCodeName = 'example-shortcode-name';
        $ajaxCallbackName = 'example-ajax-callback-name';

        \add_shortcode($shortCodeName, [$this, 'exampleShortcode']);
        \add_action('wp_ajax_nopriv_' . $ajaxCallbackName, [$this, 'exampleAjaxCallback']);
        \add_action('wp_ajax_' . $ajaxCallbackName, [$this, 'exampleAjaxCallback']);
    }
}
