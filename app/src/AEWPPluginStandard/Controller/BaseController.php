<?php

namespace AEWPPluginStandard\Controller;

use AEWPPluginStandard\Util\Configuration;
use AEWPPluginStandard\Util\ServiceManager;
use AEWPPluginStandard\Plugin;

if (!class_exists(__NAMESPACE__ . 'BaseController')) {
    /**
     * Class BaseController
     * @package AEWPPluginStandard\Controller
     */
    abstract class BaseController
    {

        private $loggerEnabled;

        /**
         * @var Configuration
         */
        private $config;

        /**
         * @var ServiceManager
         */
        private $sm;

        /**
         * Register actions and shortcodes here
         * @return BaseController
         */
        abstract public function registerHookCallbacks();

        /**
         * BaseController constructor.
         */
        public function __construct()
        {
            $this->config = new Configuration();
            $this->sm = new ServiceManager();
            $this->registerHookCallbacks();
        }

        /**
         * Render a template
         *
         * Allows parent/child themes to override the markup by placing the a file named basename( $default_template_path ) in their root folder,
         * and also allows plugins or themes to override the markup by a filter. Themes might prefer that method if they place their templates
         * in sub-directories to avoid cluttering the root folder. In both cases, the theme/plugin will have access to the variables so they can
         * fully customize the output.
         *
         * @param  string $default_template_path The path to the template, relative to the plugin's `views` folder
         * @param  array $variables An array of variables to pass into the template's scope, indexed with the variable name so that it can be extract()-ed
         * @param  string $require 'once' to use require_once() | 'always' to use require()
         *
         * @return string
         */
        public function renderTemplate($default_template_path, $variables = array(), $require = 'once')
        {
            $template_path = dirname(dirname(dirname(__DIR__))) . '/views/' . $default_template_path;
            $template_path = apply_filters(Plugin::PREFIX . 'template_path', $template_path);
            if (is_file($template_path)) {
                extract($variables);
                ob_start();

                if ('always' == $require) {
                    require($template_path);
                } else {
                    require_once($template_path);
                }

                $template_content = apply_filters(Plugin::PREFIX
                    . 'template_content', ob_get_clean(), $default_template_path, $template_path, $variables);
            } else {
                $template_content = false;
            }

            return $template_content;
        }

        /**
         * @return Configuration
         */
        public function getConfig()
        {
            return $this->config;
        }

        /**
         * @return ServiceManager
         */
        public function getServiceManager()
        {
            return $this->sm;
        }
    }
}
