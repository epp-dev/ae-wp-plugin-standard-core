<?php

namespace AEWPPluginStandard;

use AEWPPluginStandard\Controller\AdminSettingsController;
use AEWPPluginStandard\Controller\ExampleController;
use AEWPPluginStandard\Util\Configuration;
use AEWPPluginStandard\Enum\ModulesEnum;

/**
 * Class Plugin
 * @package AEWPPluginStandard
 */
class Plugin
{

    const PREFIX = 'ae-wp-plugin-standard_';

    /**
     * @var array
     * all modules (Controller objects) after initialization
     */
    protected $modules;

    /**
     * @var string
     */
    private $version;

    /**
     * Constructor
     * @mvc Controller
     * @author Aleksandr Epp <aleksandr.epp@gmail.com>
     */
    public function __construct()
    {
        $this->modules = [
            ExampleController::class => new ExampleController()
        ];
        $this->registerHookCallbacks();
    }

    /**
     * Enque registered resources dependent on current post/page
     */
    public function enqueResources()
    {
        if (is_admin()) {
            return false;
        }

        $rootDir = dirname(dirname(dirname(__FILE__)));
        $version = $this->getVersion();

        wp_register_style(
            Plugin::PREFIX . 'main',
            plugins_url('assets/css/main.css', $rootDir),
            [],
            $version,
            'all'
        );

        wp_enqueue_style(Plugin::PREFIX . 'main');

        $config = new Configuration();
        $baseUrl = $config->getBaseUrl();

        $pageId = get_the_ID();
        $pageUri = get_page_uri($pageId);

        $scriptName = null;

        if (\in_array($pageId, $config->getPID(ModulesEnum::EXAMPLE))) {
            $scriptName = ModulesEnum::EXAMPLE;
        }

        if ($scriptName !== null) {
            \wp_register_script(
                Plugin::PREFIX . $scriptName,
                \plugins_url('assets/js/' . $scriptName . '.js', $rootDir),
                [
                    // deps
                ],
                $version,
                true
            );
            \wp_enqueue_script(Plugin::PREFIX . $scriptName);
            \wp_localize_script(Plugin::PREFIX . $scriptName, 'AeWpPluginStandardAjax', [
                'baseUrl' => $baseUrl,
                'ajaxUrl' => \admin_url('admin-ajax.php')
            ]);
        }
        do_action(Plugin::PREFIX . 'enqueue_scripts');
        return $this;
    }

    /**
     * Prepares sites to use the plugin during single or network-wide activation
     *
     * @param bool $network_wide
     */
    public function activate($network_wide)
    {
    }

    /**
     * Rolls back activation procedures when de-activating the plugin
     */
    public function deactivate()
    {
    }

    public function enableSettingsPage()
    {
        // check user capabilities and add options page if he has necessary access rights
        if (current_user_can('manage_options')) {
            $adminSettingsController = new AdminSettingsController();
        }
    }

    public function enableSession()
    {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        if (null === $this->version) {
            $rootDir = dirname(dirname(dirname(__FILE__)));
            $this->version = json_decode(file_get_contents(dirname($rootDir) . '/package.json'))->version;
        }
        return $this->version;
    }

    /**
     * Register callbacks for actions and filters
     */
    public function registerHookCallbacks()
    {
        add_action('init', [$this, 'enableSession'], 1);
        add_action('init', [$this, 'enableSettingsPage']);
        add_action('wp_enqueue_scripts', [$this, 'enqueResources'], 1);
        return $this;
    }
}
