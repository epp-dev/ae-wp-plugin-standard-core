<?php
/**
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 25.08.17 15:31
 */

namespace AEWPPluginStandard\Entity;

/**
 * Class BaseEntity
 * @package AEWPPluginStandard\Entity
 */
abstract class BaseEntity
{
    /**
     * @var \DateTime
     */
    public $createdAt;

    /**
     * @var \DateTime
     */
    public $editedAt;
}
