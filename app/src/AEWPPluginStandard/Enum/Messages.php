<?php

namespace AEWPPluginStandard\Enum;

if (!\class_exists(__NAMESPACE__ . 'Messages')) {
    /**
     * Class Messages
     * @package AEWPPluginStandard\Enum
     */
    class Messages
    {
        const EXAMPLE_MESSAGE = 'Example message about <strong>%s</strong>.';

        const EXAMPLE_EXCEPTION_MESSAGE =
            // @formatter:off
            '<div class="alert alert-warning">
                <strong>Error occurred:</strong> %s
            </div>.';
            // @formatter:on

        /**
         * BEGIN: Admin settings
         */
        // fields labels
        const FIELD1_LABEL = 'Label 1';
        const FIELD2_LABEL = 'Label 2';
        const FIELD3_LABEL = 'Label 3';

        // notes
        const SECTION_DESCRIPTION = 'Settings section description';
    }

}
