<?php

namespace AEWPPluginStandard\Enum;

use AEWPPluginStandard\Plugin;

/**
 * Class AdminSettingsFieldsEnum
 * @package CapitaloCh\Enum
 */
class AdminSettingsFieldsEnum
{
    const PLUGIN_OPTIONS = Plugin::PREFIX . 'options';

    const SECTION_ID = Plugin::PREFIX . 'section_one';
    const SECTION_NAME = 'Settings section ection one';

    const FIELD1_NAME = Plugin::PREFIX . 'field-one';
    const FIELD2_NAME = Plugin::PREFIX . 'field-two';
    const FIELD3_NAME = Plugin::PREFIX . 'field-three';
}
