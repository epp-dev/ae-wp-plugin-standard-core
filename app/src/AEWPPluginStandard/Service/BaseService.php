<?php
/**
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 25.08.17 13:14
 */

namespace AEWPPluginStandard\Service;

use AEWPPluginStandard\Util\Configuration;
use AEWPPluginStandard\Plugin;

/**
 * Class BaseRestfulService
 * @package AEWPPluginStandard\Service
 */
class BaseService implements BaseServiceInterface
{
    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var array
     */
    private $httpParameters;

    /**
     * BaseRestfulService constructor.
     */
    public function __construct()
    {
        $this->config = new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * {@inheritdoc}
     */
    public function renderTemplate($defaultTemplatePath, $variables = [], $require = 'once')
    {
        $templatePath = dirname(dirname(dirname(__DIR__))) . '/views/' . $defaultTemplatePath;
        $templatePath = apply_filters(Plugin::PREFIX . 'template_path', $templatePath);
        if (is_file($templatePath)) {
            extract($variables);
            ob_start();

            if ('always' == $require) {
                require($templatePath);
            } else {
                require_once($templatePath);
            }

            $templateContent = apply_filters(Plugin::PREFIX
                . 'template_content', ob_get_clean(), $defaultTemplatePath, $templatePath, $variables);
        } else {
            $templateContent = false;
        }

        return $templateContent;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpParameters()
    {
        return $this->httpParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function setHttpParameters($httpParameters)
    {
        $this->httpParameters = $httpParameters;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addHttpParameter($name, $value)
    {
        if (is_array($value)) {
            $this->httpParameters[$name] = array_merge($value, $this->httpParameters[$name]);
        } else {
            $this->httpParameters[$name] = $value;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCookie($key, $value, $expire = 3600, $path = '/')
    {
        \setcookie(
            $key,
            $value,
            time() + $expire,
            $path
        );
    }

    /**
     * {@inheritdoc}
     */
    public function deleteCookie($key, $path = '/')
    {
        // save selected insurance in a cookie
        \setcookie(
            $key,
            '',
            time() - 3600,
            $path
        );
    }
}
