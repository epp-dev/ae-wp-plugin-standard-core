<?php
/**
 * Created by Aleksandr Epp<aleksandr.epp@gmail.com>
 * date: 25.08.17 13:10
 */

namespace AEWPPluginStandard\Service;

use AEWPPluginStandard\Util\Configuration;

/**
 * Interface BaseServiceInterface
 * @package AEWPPluginStandard\Service
 */
interface BaseServiceInterface
{
    /**
     * @return Configuration
     */
    public function getConfig();

    /**
     * Render a template
     *
     * Allows parent/child themes to override the markup by placing the a file named basename( $default_template_path ) in their root folder,
     * and also allows plugins or themes to override the markup by a filter. Themes might prefer that method if they place their templates
     * in sub-directories to avoid cluttering the root folder. In both cases, the theme/plugin will have access to the variables so they can
     * fully customize the output.
     *
     * @param  string $defaultTemplatePath The path to the template, relative to the plugin's `views` folder
     * @param  array $variables An array of variables to pass into the template's scope, indexed with the variable name so that it can be extract()-ed
     * @param  string $require 'once' to use require_once() | 'always' to use require()
     *
     * @return string
     */
    public function renderTemplate($defaultTemplatePath, $variables = [], $require = 'once');

    /**
     * @return array
     */
    public function getHttpParameters();

    /**
     * @param array $httpParameters
     * @return BaseServiceInterface
     */
    public function setHttpParameters($httpParameters);

    /**
     * @param string $name
     * @param mixed $value
     * @return BaseServiceInterface
     */
    public function addHttpParameter($name, $value);

    /**
     * @param string $key
     * @param string $value
     * @param int $expire seconds from now
     * @param string $path
     * @return BaseServiceInterface
     */
    public function setCookie($key, $value, $expire = 3600, $path = '/');

    /**
     * @param string $key
     * @param string $path
     * @return BaseServiceInterface
     */
    public function deleteCookie($key, $path = '/');
}
