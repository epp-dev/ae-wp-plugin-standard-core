<?php

/*
  Plugin Name: AE WP Plugin Standard
  Plugin URI: https://www.ae-wp-plugin-standard.org
  Description: Standard WP Plugin skeleton
  Version: @@version
  Author: Aleksandr Epp
  Author URI: aleksandr.epp@gmail.com
 */

namespace AEWPPluginStandard;

if (\filter_input(INPUT_SERVER, 'SCRIPT_FILENAME') === __FILE__) {
    die('Access denied.');
}
define('AE_WP_PLUGIN_STANDARD_NAME', 'AE WP Plugin Standard');
define('AE_WP_PLUGIN_STANDARD_TECHNICAL_NAME', 'AEWPPluginStandard');
define('AE_WP_PLUGIN_STANDARD_NAME_REQUIRED_PHP_VERSION', '5.6');
define('AE_WP_PLUGIN_STANDARD_NAME_REQUIRED_WP_VERSION', '4.8');

date_default_timezone_set('Europe/Berlin');

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Checks if the system requirements are met
 * @return bool true if system requirements are met, false if not
 */
function aeWpPluginStandardRequirementsMet()
{
    global $wp_version;
    require_once(ABSPATH . '/wp-admin/includes/plugin.php');  // to get is_plugin_active() early

    if (version_compare(PHP_VERSION, AE_WP_PLUGIN_STANDARD_NAME_REQUIRED_PHP_VERSION, '<=')) {
        return false;
    }

    if (version_compare($wp_version, AE_WP_PLUGIN_STANDARD_NAME_REQUIRED_WP_VERSION, '<=')) {
        return false;
    }
    return true;
}

/**
 * Check requirements and load main class
 * The main program needs to be in a separate file that only gets loaded
 * if the plugin requirements are met.
 * Otherwise older PHP installations could crash when trying to parse it.
 */
if (aeWpPluginStandardRequirementsMet()) {
    if (class_exists(__NAMESPACE__ . '\Plugin')) {
        $GLOBALS['AEWPPluginStandard'] = new Plugin();
    }
}
