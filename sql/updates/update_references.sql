-- update testing from production
UPDATE wp_options SET option_value = replace(option_value, 'https://ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_options SET option_value = replace(option_value, 'https://www.ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET post_content = replace(post_content, 'https://ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org');
UPDATE wp_posts SET post_content = replace(post_content, 'https://www.ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'https://ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'https://www.ae-wp-plugin-standard.org', 'https://testing.ae-wp-plugin-standard.org');

-- update dev from production

UPDATE wp_options SET option_value = replace(option_value, 'https://ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_options SET option_value = replace(option_value, 'https://www.ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET post_content = replace(post_content, 'https://ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev');
UPDATE wp_posts SET post_content = replace(post_content, 'https://www.ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'https://ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'https://www.ae-wp-plugin-standard.org', 'http://ae-wp-plugin-standard.dev');